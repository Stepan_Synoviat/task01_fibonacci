/**
 * package where I save all my homeworks
 */
package task1;

import java.util.ArrayList;

/**
 * Class which makes different calculations.
 *
 * @author Stepan Synoviat
 * @version 1.0
 */
public class Calculation {
	/**
	 * the biggest odd number of Fibonacci numbers.
	 */
	private int F1;
	/**
	 * the biggest even number of Fibonacci numbers.
	 */
	private int F2;
	/**
	 * the size of set of Fibonacci numbers.
	 */
	private int n;

	/**
	 * Program prints odd numbers from start to the end of interval.
	 * Program prints the sum of odd and even numbers.
	 *
	 * @param k - the least interval value
	 * @param l - the bigest interval value
	 */
	public void printOddNumber(int k, int l) {
		if (k % 2 != 0) {
			ArrayList<Integer> oddList = new ArrayList<Integer>();
			for (int i = k; i < l; i += 2) {
				oddList.add(i);
				System.out.print(i + " ");
			}
			int sum = oddList.get(0);
			for (int i = 1; i < oddList.size(); i++) {
				sum += oddList.get(i);
			}
			System.out.println(" ");
			System.out.println("Sum of odd numbers = " + sum);
		} else {
			ArrayList<Integer> oddList2 = new ArrayList<Integer>();
			++k;
			for (int i = k; i < l; i += 2) {
				oddList2.add(i);
				System.out.print(i + " ");
			}
			int sum = oddList2.get(0);
			for (int i = 1; i < oddList2.size(); i++) {
				sum += oddList2.get(i);
			}
			System.out.println(" ");
			System.out.println("Sum of odd numbers = " + sum);
		}
	}

	/**
	 * function to buildig Fibonacci numbers.
	 *
	 * @param n -{@link Calculation#n}
	 */
	public void fibonnachiNumb(int n) {
		this.n = n;
		int[] f = new int[n];
		boolean isEmpty1 = false;
		boolean isEmpty2 = false;
		double persentageOfOddNumbr;
		double persentageOfEvenNumbr;

		int n1 = n;
		int n2 = n;
		int n3 = n;
		int n4 = n;
		f[0] = 0;
		f[1] = 1;
		for (int i = 2; i < n; ++i) {
			f[i] = f[i - 1] + f[i - 2];
			System.out.println(f[i]);
		}

		int sum2 = f[0];
		for (int i = 1; i < f.length; i++) {
			sum2 += f[i];
		}
		// checing of the bigest odd number
		while (!isEmpty1 && n1 > 0) {
			if (f[n1 - 1] % 2 != 0) {
				System.out.println("F1 = " + f[n1 - 1]);
				isEmpty1 = true;
			} else {
				n1--;
			}
		}
		// checing of the bigest even number
		while (!isEmpty2 && n2 > 0) {
			if (f[n2 - 1] % 2 == 0) {
				System.out.println("F2 = " + f[n2 - 1]);
				isEmpty2 = true;
			} else {
				n2--;
			}
		}
	}

	/**
	 * main method.
	 */
	public static void main(String[] abc) {
		Calculation test1 = new Calculation();
		int setSize = 7;
		int o = 4;
		int g = 10;
		test1.printOddNumber(o, g);
		test1.fibonnachiNumb(setSize);
	}
}


